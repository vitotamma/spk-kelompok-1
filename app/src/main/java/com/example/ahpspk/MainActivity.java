package com.example.ahpspk;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private Spinner SPHargaVsKebersihan, SPHargaVsPorsi, SPHargaVskecepatanPelayanan, SPKebersihanVsPorsi, SPKebersihanVsKecepatanPelayanan, SPPorsiVsKecepatanPelayanan;
    double valSPHK, valSPHP,valSPHKP, valSPKP, valSPKKP, valSPPKP;
    public String SPHK, SPHP,SPHKP, SPKP, SPKKP, SPPKP;
    private Button BtnSubmit;

    double matPerb[][]=new double [4][4];
    double matNorm[][]=new double [4][4];
    String alt[]={"Kantin FST","Kantin FKM","Kantin RSUA","Kantin Lapangan Futsal"};
    double matBobot[][]={{0.5,0.1,0.3,0.1},{0.1,0.3,0.1,0.5},{0.3,0.5,0.1,0.1},{0.1,0.1,0.5,0.3}};
    double scoring[]=new double[4];
    double hasil[]=new double[4];
    double totPerbH[]=new double[4];
    double totPerbV[]=new double[4];
    double konsistensi[]=new double[4];
    double averageKons;
    double CI;
    double RI=0.9;
    double CR;
    double CRPercent;
    int ranking[]=new int[4];

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        addItemsOnSpinner();
        submit();

    }
    // coba aja

    // add items into spinner dynamically
    public void addItemsOnSpinner() {

        SPHargaVsKebersihan = findViewById(R.id.SPHargaVsKebersihan);
        List<String> listSPHK = new ArrayList<String>();
        listSPHK.add("9.0");
        listSPHK.add("8.0");
        listSPHK.add("7.0");
        listSPHK.add("6.0");
        listSPHK.add("5.0");
        listSPHK.add("4.0");
        listSPHK.add("3.0");
        listSPHK.add("2.0");
        listSPHK.add("1.0");
        listSPHK.add("0.5");
        listSPHK.add("0.33333");
        listSPHK.add("0.25");
        listSPHK.add("0.2");
        listSPHK.add("0.16667");
        listSPHK.add("0.125");
        listSPHK.add("0.11111");
        ArrayAdapter<String> dataAdapterSPHK = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, listSPHK);
        dataAdapterSPHK.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        SPHargaVsKebersihan.setAdapter(dataAdapterSPHK);


        SPHargaVsPorsi = findViewById(R.id.SPHargaVsPorsi);
        List<String> listSPHP = new ArrayList<String>();
        listSPHP.add("9.0");
        listSPHP.add("8.0");
        listSPHP.add("7.0");
        listSPHP.add("6.0");
        listSPHP.add("5.0");
        listSPHP.add("4.0");
        listSPHP.add("3.0");
        listSPHP.add("2.0");
        listSPHP.add("1.0");
        listSPHP.add("0.5");
        listSPHP.add("0.33333");
        listSPHP.add("0.25");
        listSPHP.add("0.2");
        listSPHP.add("0.16667");
        listSPHP.add("0.125");
        listSPHP.add("0.11111");
        ArrayAdapter<String> dataAdapterSPHP = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, listSPHP);
        dataAdapterSPHP.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        SPHargaVsPorsi.setAdapter(dataAdapterSPHP);

        SPHargaVskecepatanPelayanan = findViewById(R.id.SPHargaVskecepatanPelayanan);
        List<String> listSPHKP = new ArrayList<String>();
        listSPHKP.add("9.0");
        listSPHKP.add("8.0");
        listSPHKP.add("7.0");
        listSPHKP.add("6.0");
        listSPHKP.add("5.0");
        listSPHKP.add("4.0");
        listSPHKP.add("3.0");
        listSPHKP.add("2.0");
        listSPHKP.add("1.0");
        listSPHKP.add("0.5");
        listSPHKP.add("0.33333");
        listSPHKP.add("0.25");
        listSPHKP.add("0.2");
        listSPHKP.add("0.16667");
        listSPHKP.add("0.125");
        listSPHKP.add("0.11111");
        ArrayAdapter<String> dataAdapterSPHKP = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, listSPHKP);
        dataAdapterSPHKP.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        SPHargaVskecepatanPelayanan.setAdapter(dataAdapterSPHKP);

        SPKebersihanVsPorsi = findViewById(R.id.SPKebersihanVsPorsi);
        List<String> listSPKP = new ArrayList<String>();
        listSPKP.add("9.0");
        listSPKP.add("8.0");
        listSPKP.add("7.0");
        listSPKP.add("6.0");
        listSPKP.add("5.0");
        listSPKP.add("4.0");
        listSPKP.add("3.0");
        listSPKP.add("2.0");
        listSPKP.add("1.0");
        listSPKP.add("0.5");
        listSPKP.add("0.33333");
        listSPKP.add("0.25");
        listSPKP.add("0.2");
        listSPKP.add("0.16667");
        listSPKP.add("0.125");
        listSPKP.add("0.11111");
        ArrayAdapter<String> dataAdapterSPKP = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, listSPKP);
        dataAdapterSPKP.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        SPKebersihanVsPorsi.setAdapter(dataAdapterSPKP);

        SPKebersihanVsKecepatanPelayanan = findViewById(R.id.SPKebersihanVsKecepatanPelayanan);
        List<String> listSPKKP = new ArrayList<String>();
        listSPKKP.add("9.0");
        listSPKKP.add("8.0");
        listSPKKP.add("7.0");
        listSPKKP.add("6.0");
        listSPKKP.add("5.0");
        listSPKKP.add("4.0");
        listSPKKP.add("3.0");
        listSPKKP.add("2.0");
        listSPKKP.add("1.0");
        listSPKKP.add("0.5");
        listSPKKP.add("0.33333");
        listSPKKP.add("0.25");
        listSPKKP.add("0.2");
        listSPKKP.add("0.16667");
        listSPKKP.add("0.125");
        listSPKKP.add("0.11111");
        ArrayAdapter<String> dataAdapterSPKKP = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, listSPKKP);
        dataAdapterSPKKP.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        SPKebersihanVsKecepatanPelayanan.setAdapter(dataAdapterSPKKP);

        SPPorsiVsKecepatanPelayanan = findViewById(R.id.SPPorsiVsKecepatanPelayanan);
        List<String> listSPPKP = new ArrayList<String>();
        listSPPKP.add("9.0");
        listSPPKP.add("8.0");
        listSPPKP.add("7.0");
        listSPPKP.add("6.0");
        listSPPKP.add("5.0");
        listSPPKP.add("4.0");
        listSPPKP.add("3.0");
        listSPPKP.add("2.0");
        listSPPKP.add("1.0");
        listSPPKP.add("0.5");
        listSPPKP.add("0.33333");
        listSPPKP.add("0.25");
        listSPPKP.add("0.2");
        listSPPKP.add("0.16667");
        listSPPKP.add("0.125");
        listSPPKP.add("0.11111");
        ArrayAdapter<String> dataAdapterSPPKP = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, listSPPKP);
        dataAdapterSPPKP.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        SPPorsiVsKecepatanPelayanan.setAdapter(dataAdapterSPPKP);
    }


    public void AHP(){
        SPHargaVsKebersihan = findViewById(R.id.SPHargaVsKebersihan);
        SPHargaVsPorsi = findViewById(R.id.SPHargaVsPorsi);
        SPHargaVskecepatanPelayanan = findViewById(R.id.SPHargaVskecepatanPelayanan);
        SPKebersihanVsPorsi = findViewById(R.id.SPKebersihanVsPorsi);
        SPKebersihanVsKecepatanPelayanan = findViewById(R.id.SPKebersihanVsKecepatanPelayanan);
        SPPorsiVsKecepatanPelayanan = findViewById(R.id.SPPorsiVsKecepatanPelayanan);

            for(int i=0;i<matPerb.length;i++){
                for(int j=0;j<matPerb.length;j++){
                    if(i==j)
                        matPerb[i][j]=1;
                }
            }
            this.SPHK = String.valueOf(SPHargaVsKebersihan.getSelectedItem());
            this.valSPHK = Double.parseDouble(SPHK);
            matPerb[0][1]=valSPHK;
            matPerb[1][0]=1/valSPHK;

            this.SPHP = String.valueOf(SPHargaVsPorsi.getSelectedItem());
            this.valSPHP = Double.parseDouble(SPHP);
            matPerb[0][2]=valSPHP;
            matPerb[2][0]=1/valSPHP;

            this.SPHKP = String.valueOf(SPHargaVskecepatanPelayanan.getSelectedItem());
            this.valSPHKP = Double.parseDouble(SPHKP);
            matPerb[0][3]=valSPHKP;
            matPerb[3][0]=1/valSPHKP;

            this.SPKP = String.valueOf(SPKebersihanVsPorsi.getSelectedItem());
            this.valSPKP = Double.parseDouble(SPKP);
            matPerb[1][2]=valSPKP;
            matPerb[2][1]=1/valSPKP;

            this.SPKKP = String.valueOf(SPKebersihanVsKecepatanPelayanan.getSelectedItem());
            this.valSPKKP = Double.parseDouble(SPKKP);
            matPerb[1][3]=valSPKKP;
            matPerb[3][1]=1/valSPKKP;

            this.SPPKP = String.valueOf(SPPorsiVsKecepatanPelayanan.getSelectedItem());
            this.valSPPKP = Double.parseDouble(SPPKP);
            matPerb[2][3]=valSPPKP;
            matPerb[3][2]=1/valSPPKP;

            for(int i=0;i<matPerb.length;i++){
                for(int j=0;j<matPerb.length;j++){
                    totPerbH[i]+=matPerb[i][j];
                    totPerbV[i]+=matPerb[j][i];
                }
            }

            //Matrix Normalisasi
            for(int i=0;i<matPerb.length;i++){
                for(int j=0;j<matPerb.length;j++){
                    matNorm[j][i]=matPerb[j][i]/totPerbV[i];
                }
            }
            //scoring
            double sum=0;
            for(int i=0;i<matPerb.length;i++){
                for(int j=0;j<matPerb.length;j++){
                    sum+=matNorm[i][j];
                }
                scoring[i]=sum/4;
                sum=0;
            }

            //pengecekan konsistensi
            double sum2=0;
            for(int i=0;i<matPerb.length;i++){
                for(int j=0;j<matPerb.length;j++){
                    sum2+=(matPerb[i][j]*scoring[j]);
                }
                konsistensi[i]=sum2/scoring[i];
                sum2=0;
            }


            //rata" konsistensi
            double sumK=0;
            for(int i=0;i<konsistensi.length;i++){
                sumK+=konsistensi[i];
            }
            averageKons=sumK/konsistensi.length;

            //CI
            CI=(averageKons-konsistensi.length)/(konsistensi.length-1);
            //CR
            CR=CI/RI;
            CRPercent=CR*100;

            if(CRPercent<10){
                Toast.makeText(MainActivity.this, "Data Konsisten", Toast.LENGTH_SHORT).show();
            }
            else {
                Toast.makeText(MainActivity.this, "Data Tidak Konsisten", Toast.LENGTH_SHORT).show();
            }

            //hasil
            double sum3=0;
            for(int i=0;i<matBobot.length;i++){
                for(int j=0;j<matBobot.length;j++){
                    sum3+=(matBobot[i][j]*scoring[j]);
                }
                hasil[i]=sum3;
                sum3=0;
            }
            //perangkingan
            double tempH[]=new double[4];
            for(int i=0;i<tempH.length;i++){
                tempH[i]=hasil[i];
            }

            double max=0;
            int countRank=0;
            int ind=0;
            while(countRank<4){
                for(int i=0;i<tempH.length;i++){
                    if(tempH[i]>max){
                        max=tempH[i];
                        ind=i;
                    }
                }
                max=0;
                tempH[ind]=0;
                ranking[countRank]=ind;
                countRank++;
            }

    }


    public void submit() {
        final Button BtnSubmit = findViewById(R.id.BtnSubmit);
        BtnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AHP();

                String rank1 = alt[ranking[0]];
                String rank2 = alt[ranking[1]];
                String rank3 = alt[ranking[2]];

                Double hasil1 = hasil[ranking[0]];
                Double hasil2 = hasil[ranking[1]];
                Double hasil3 = hasil[ranking[2]];

                Intent intent = new Intent(MainActivity.this, ranking.class);
                intent.putExtra("Nama Kantin 1", rank1);
                intent.putExtra("Nama Kantin 2", rank2);
                intent.putExtra("Nama Kantin 3", rank3);

                intent.putExtra("Nilai Kantin 1", hasil1);
                intent.putExtra("Nilai Kantin 2", hasil2);
                intent.putExtra("Nilai Kantin 3", hasil3);
                startActivity(intent);
            }
        });

    }
}
