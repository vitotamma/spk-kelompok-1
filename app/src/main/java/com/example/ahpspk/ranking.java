package com.example.ahpspk;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class ranking extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ranking);

        Intent intent = getIntent();
        String rank1 = intent.getStringExtra("Nama Kantin 1");
        String rank2 = intent.getStringExtra("Nama Kantin 2");
        String rank3 = intent.getStringExtra("Nama Kantin 3");

        Double hasil1 =  intent.getDoubleExtra("Nilai Kantin 1",0.0);
        Double hasil2 =  intent.getDoubleExtra("Nilai Kantin 2",0.0);
        Double hasil3 =  intent.getDoubleExtra("Nilai Kantin 3",0.0);


        TextView nilai1 = findViewById(R.id.nilaiRanking1);
        nilai1.setText(hasil1.toString());

        TextView nilai2 = findViewById(R.id.nilaiRanking2);
        nilai2.setText(hasil2.toString());

        TextView nilai3 = findViewById(R.id.nilaiRanking3);
        nilai3.setText(hasil3.toString());

        TextView kantinRank1 = findViewById(R.id.kantinRanking1);
        kantinRank1.setText(rank1);

        TextView kantinRank2 = findViewById(R.id.kantinRanking2);
        kantinRank2.setText(rank2);

        TextView kantinRank3 = findViewById(R.id.kantinRanking3);
        kantinRank3.setText(rank3);

    }
}
